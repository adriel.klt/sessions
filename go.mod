module gitlab.com/adriel.klt/sessions

go 1.15

require (
	github.com/antonlindstrom/pgstore v0.0.0-20220421113606-e3a6e3fed12a
	github.com/bos-hieu/mongostore v0.0.3
	github.com/bradfitz/gomemcache v0.0.0-20230124162541-5f7a7d875746
	github.com/bradleypeabody/gorilla-sessions-memcache v0.0.0-20181103040241-659414f458e1
	github.com/gin-gonic/gin v1.8.2
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/gorilla/context v1.1.1
	github.com/gorilla/sessions v1.2.1
	github.com/kidstuff/mongostore v0.0.0-20181113001930-e650cd85ee4b
	github.com/memcachier/mc v2.0.1+incompatible
	github.com/quasoft/memstore v0.0.0-20191010062613-2bce066d2b0b
	github.com/wader/gormstore/v2 v2.0.3
	go.mongodb.org/mongo-driver v1.11.1
	gorm.io/driver/sqlite v1.4.4
	gorm.io/gorm v1.24.5
)

require (
	github.com/gin-contrib/sessions v0.0.5
	github.com/go-playground/validator/v10 v10.11.2 // indirect
	github.com/go-redis/redis/v8 v8.11.5
	github.com/goccy/go-json v0.10.0 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/gorilla/securecookie v1.1.1
	github.com/klauspost/compress v1.15.15 // indirect
	github.com/lib/pq v1.10.7 // indirect
	github.com/mattn/go-isatty v0.0.17 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/montanaflynn/stats v0.7.0 // indirect
	github.com/ugorji/go/codec v1.2.9 // indirect
	github.com/xdg-go/scram v1.1.2 // indirect
	github.com/youmark/pkcs8 v0.0.0-20201027041543-1326539a0a0a // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/sys v0.5.0 // indirect
)
